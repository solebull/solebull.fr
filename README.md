# solebull.fr

Un (très) simple site web de présentation pour notre activité de *streamers*.

## Installation

Copier le contenu du répertoire **www/** dans la racine de votre hébergement
web.

## Nanoc version

### On debian

If your system doesn't provide ruby 2.4 (for example, Debian Stretch), you
must install ruby 2.4 before installing dependencies :

	sudo apt-get install curl 
	curl -sSL https://rvm.io/mpapis.asc | sudo gpg --import -
	curl -sSL https://get.rvm.io | sudo bash -s stable
	su 
	#source /etc/profile.d/rvm.sh
	#rvm requirements
	#rvm list known
	#rvm install 2.7.4
	#sudo chmod a+w -R /usr/local/rvm/gems/ruby-2.4.4/wrappers
	exit
	rvm use 2.7.4 --default

To install needed gems, especially *nanoc* one, you must to run :

	sudo apt install bundler
	bundle config set --local path 'vendor/bundle'
	bundle install

Then build and test your website :

	cd src
	nanoc
	nanoc view

### On manjaro and arch-based distributions

	sudo pacman -S ruby-bundler
	bundle config set --local path 'vendor/bundle'
	bundle install

# Configuration

The *minetest-pvp* page can be hidden in `src/nanoc.yaml`, with the 
**minetest_show** value.

The *planning* menu item can be hidden setting the **planning_show** calue
in the same config file.

# Unit test

From the root directory :

	make check

# Code documentation

Use the following command to generate documentation (you need `yard`).

	make doc
	
Then, open `doc/index.html`	with your favorite browser.

# Troubleshooting

## Adding a new game doesn't add it to the games list

You may need to manually remove the `src/www/games/index.html` file and run
`nanoc` again.
