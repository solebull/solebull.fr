YARD=./vendor/bundle/ruby/2.7.0/gems/yard-0.9.27/bin/yard

# Force rebuild of these targets
.PHONY: doc clean

# The 2>/dev/null redirect stderr to nothing, so if you use
# warn() method of ruby in implementation, it shouldn't be shown
# during tests
check:
	@cd src && ruby test/tests.rb # 2>/dev/null

doc:
	$(YARD) doc src/
	$(YARD) stats src/ --list-undoc

clean:
	rm -fr doc
