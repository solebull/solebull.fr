---
title: osu!
playlist: 
layout: /game.*
menu: games
---

## osu! ##

osu! est un jeu de rythme gratuit pour Windows, Mac OS et Linux 
(via Mono et Wine), créé par Dean «peppy» Herbert, écrit en C#, et inspiré 
du jeu Osu! Tatakae! Ōendan (connu en occident sous le nom d'Elite Beat 
Agents) sur Nintendo DS. Il a été lancé le 16 septembre 2007 en bêta ouverte.

Nous ne streamerons ce jeu que chez *twitch* a cause de la musique.
