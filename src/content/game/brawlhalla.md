---
title:  Brawlhalla
playlist: 
layout: /game.*
menu: games
---

## Brawlhalla ##

Brawlhalla est un jeu de combat 2D gratuit développé par Blue Mammoth Games 
Le jeu a été présenté en alpha en Mai 2014. 
Une version bêta ouverte est disponible en novembre 2015, 
suivie de la sortie du jeu en octobre 2017. 
