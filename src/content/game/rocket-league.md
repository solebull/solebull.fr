---
title: Rocket League
playlist: PLk_fUHaG1xof37TYGpS3ISqKBVSv_jQEK
layout: /game.*
menu: games
---

## Rocket League ##

Rocket League est un jeu vidéo de sport développé par Psyonix, sorti en 
juillet 2015 sur Windows et sur PlayStation 4, en février 2016 sur Xbox One, 
en septembre 2016 sur Linux et Mac et en novembre 2017 sur Nintendo Switch.
