---
title: "The Elder Scrolls V: Skyrim"
playlist: PLk_fUHaG1xodaBg_aFmRGTkc3BnRoRJsM
layout: /game.*
menu: games
---

## The Elder Scrolls V: Skyrim ##


*The Elder Scrolls V: Skyrim* (souvent abrégé en *Skyrim*) est un jeu vidéo 
de rôle et d'action développé par *Bethesda Game Studios* et édité par 
*Bethesda Softworks*, sorti le 28 octobre 2016 sur PS4.
