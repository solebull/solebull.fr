---
title: "Detroit: Become Human"
playlist: PLk_fUHaG1xofVUmkq505UhGRE0RNextug
layout: /game.*
menu: games
---

## Detroit: Become Human ##

*Detroit: Become Human* est un jeu vidéo développé par Quantic Dream, édité par 
Sony et qui paraît le 25 mai 2018 sur PlayStation 4.
