---
title: GTA V
playlist: PLk_fUHaG1xoe-7RSKRwVhdqfjuYjKQMsM
layout: /game.*
menu: games
---

## GTA V ##

*Grand Theft Auto V* (plus communément abrégé GTA V) est un jeu vidéo 
d'action-aventure, développé par *Rockstar North* et édité par *Rockstar Games*
sortit le 18 novembre 2014 sur PS4.
