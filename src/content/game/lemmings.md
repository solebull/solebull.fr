---
title: Lemmings
playlist: PLk_fUHaG1xofuQfYXxEX9VBS9KCtOMwRD
layout: /game.*
menu: games
---

## Lemmings ##

Lemmings est un jeu vidéo de réflexion développé par le studio écossais 
DMA Design, aujourd'hui Rockstar North et édité par Psygnosis en 1991.

Nous jouons a la version Atari ST et le jeu fait partie de la playlist 
*Retro gaminig*.
