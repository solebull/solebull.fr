---
title:  Rainbow Six Siege
playlist: PLk_fUHaG1xodjS0gmRWR2TJ6tCPvVF1ve
layout: /game.*
menu: games
---

## Tom Clancy's Rainbow Six: Siege ##

Tom Clancy's Rainbow Six: Siege, souvent abbrégé en *R6*, est un jeu vidéo de 
tir tactique développé par *Ubisoft Montréal* et édité par Ubisoft, sorti le 
1<sup>er</sup> décembre 2015 sur PlayStation 4, Xbox One et Windows.
Ce jeu nous a été offert par 
[Tekdor](https://www.youtube.com/channel/UC6PKEnk_es0B6wXH9l7xNPQ/).
