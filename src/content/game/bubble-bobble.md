---
title: Bubble Bobble
playlist: PLk_fUHaG1xofuQfYXxEX9VBS9KCtOMwRD
layout: /game.*
menu: games
---

## Bubble Bobble ##

Bubble Bobble est un jeu vidéo de plates-formes développé par la société japonaise Taito, sorti en 1986. 

La playlist concerne tous les jeux rétro.
