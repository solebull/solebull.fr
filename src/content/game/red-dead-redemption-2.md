---
title: "Red Dead Redemption 2"
playlist: PLk_fUHaG1xodrnzy3nfOMFbxzdTfZtChT
layout: /game.*
menu: games
---

## Red Dead Redemption 2 ##

*Red Dead Redemption 2* est un jeu vidéo d'action-aventure et de western 
multiplateforme, développé par Rockstar Studios et édité par Rockstar Games, 
sorti le 26 octobre 2018 sur PlayStation 4.
