---
title:  Minecraft
playlist: PLk_fUHaG1xofwpvtBgJky4pBhZK73-cbb
layout: /game.*
menu: games
---

## Minecraft ##

Minecraft est un jeu vidéo de type « bac à sable » se jouant dans un monde fait de *Voxels*, développé par le Suédois Markus Persson, alias Notch, puis par 
le studio de développement *Mojang*. 

Nous ne jouons qu'en mode survie. La saison une est terminée depuis longtemps.


<iframe width="560" height="315" src="https://www.youtube.com/embed/videoseries?list=PLk_fUHaG1xoffbNOSzQ6yRSQlBfxzABu_" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
