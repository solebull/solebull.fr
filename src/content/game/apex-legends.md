---
title: Apex Legends
playlist: PLk_fUHaG1xoeG45ty3dpZmwCvKUjODnic
layout: /game.*
menu: games
---

## Apex Legends ##

Apex Legends est un jeu vidéo de type battle royale développé par 
*Respawn Entertainment* et édité par *Electronic Arts*. Il est publié en 
accès gratuit le 4 février 2019 sur Microsoft Windows, PlayStation 4 et 
Xbox One.
