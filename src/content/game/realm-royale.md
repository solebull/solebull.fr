---
title:  Realm Royale
playlist: PLk_fUHaG1xocMbTUz_IgfFVKQpAaE4d-H
layout: /game.*
menu: games
---

## Realm Royale ##

Realm Royale est un jeu de *Battle Royale* gratuit développé par 
Heroic Leap Games. Le jeu propose plusieurs classes de personnages, 
chacune avec des capacités uniques. Il s'agit d'une spin-off du jeu Paladins, 
où il était à l'origine un mode de jeu appelé Paladins: Battlegrounds. Le jeu
est sortit sur PlayStation 4 an Aout 2018.
