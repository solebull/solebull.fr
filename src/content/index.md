---
title: "Solebull et SniperGames : streamers gaming PS4"
menu: acceuil
---

Salut les amis, c'est *SniperGames* et *Solebull*, on stream sur
[Youtube](https://www.youtube.com/channel/UCAmdo3BvFP7V0X1Djl8pV4g) et
[Twitch](https://www.twitch.tv/solebull)
tous les jours sauf le jeudi et le dimanche 18h a 22h.
    
On stream sur PS4 des jeux comme *Minecraft*, *Fortnite*, *Diablo III*, 
*Rocket League*, *Red Dead Redemption 2* etc...

On stream aussi des jeux *retro-gaming* de l'Atari ST : *Bubble Bobble*, 
*Dungeon Master*, les jeux *Lankhor*  etc...

## Setup

Streaming *Playstation*

  * PS4 Pro 1To;
  * Playstation Camera v2.

Pour le *retro-gaming*

  * Quad-Core AMD 4100 3.6Ghz;
  * 10 Go RAM;
  * 1 To HD;
  * Nvidia GeForce GTX 260;
  * OS : Debian GNU/Linux Buster;
  * Streaming : OBS 21.1.2;
  * Emulateur Atari : Hatari 2.0.0;

Setup commun :

  * Micro Auna MIC-900S USB;

## Contact

Pour nous contacter en privé :

  * Discord : [discord.gg/6krXCG](https://discord.gg/6krXCG)
  * Twitter : [@solebull42](https://twitter.com/Solebull42);
  * Facebook : [solebull42](https://www.facebook.com/solebull42/)
  * Mail : [solebull42&lt;at&gt;gmail&lt;dot&gt;com](mailto:solebull42@gmail.com)
  * PSN : **SnipperGames** et **solebull**.
  * Pseudo *epic* (Fortnite) : **solebull**
