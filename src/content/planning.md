---

title: "Planning | Solebull et SniperGames"
layout: /planning.*
menu: planning

games:
- [ 2 Septembre 2019, "Little Big Planet", "yt"]
- [ 3 Septembre 2019, "Red Dead Redemption 2", "yt"]
- [ 4 Septembre 2019, "Realm Royale", "yt"]
- [ 5 Septembre 2019, "", "rest"]
- [ 6 Septembre 2019, "Apex Legends", "yt"]
- [ 7 Septembre 2019, "Fortnite", "yt"]
---

Voici le planning de la semaine. Les lives se déroulent generalement de 18h à 
22h, heures de Paris. Ne nous demandez pas de changer de jeu.

Il m'arrive quelque fois de changer en cours de soirée pour faire
du retro-gaming ou vous présenter des jeux libres.
