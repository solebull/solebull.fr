require 'stringio'


# Add a menu item in the branding menu
#
# title : the printed text
# link  : the url
#
# We can also define disabled link
#  <!-- <li class="nav-item">
#          <a class="nav-link disabled" href="#">Disabled</a>
#       </li>
#  -->
def menu_item(title, link)

  if @item[:menu] == title.downcase
    active = "active"
  else
    active = ""
  end

  ## Special case liste de jeux
  if @item[:menu]== "games" and  title == "Liste de jeux"
    active = "active"
  end    
  
  io = StringIO.new

  io << "<li class='nav-item #{active}'>"
  io << "<a class='nav-link' href='/#{link}'>#{title}"
  io << "<span class='sr-only'>(current)</span></a>"
  io << "</li>"

  return io.string
end

# Return all the menu items
#
# It only show several pages due to config file settings
def menu
  items = menu_item("Accueil", "")
  
  if @config[:planning_show] then
    items = items + menu_item("Planning", "planning/")
  end

  items = items + menu_item("Liste de jeux", "games/")

  if @config[:minetest_show] then
    items = items + menu_item("Minetest-pvp", "minetest-pvp/")
  end
  
  return  items
         
  # +  menu_item("Donateurs", "donateurs/")
end
