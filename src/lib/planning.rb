# A special helper for planning handling

#require Date
require 'stringio'

# Here are the valid services
$services = ["yt", "twitch", "rest"]

# Returns a new div row definition
def new_row
  return "<div class='row'>"
end

# Returns a new div col definition with the given style
#
# text:: The printed text
# style:: The CSS class(es)
def new_col(text, style)
  return "<div class='col-xs #{style}'>#{text}</div>"
end

# Returns a target _blank link
#
# This link will open in a new browser tab
def service_link(url, service_name)
  return "<a href='#{url}' target='_blank'>#{service_name}</a>"
end

## Check if the 3rd element of the array is a valid service
#
# The item array must contain ["date", "game", <service name or rest>]. If the
# third element (index is 2) is not "yt" or "twitch", the service is raised
# as invalid.
def check_service(item)
  if item[2].nil?
    raise "Planning Item #{item[0]} - #{item[1]} doesn't have a service! see src/content/planning.md metadata for more."
  end

  if not $services.include? item[2]
    raise "Planning Item #{item[2]} doesn't exist"
  end

  # Then, return to text
  if item[2] == "yt"
    service_link('https://www.youtube.com/channel/UCAmdo3BvFP7V0X1Djl8pV4g',
                 "Youtube")
  else
    return service_link('https://www.twitch.tv/solebull', "Twitch")
  end
end


# All in one column
def planning
  days = ["Lundi", "Mardi", "Mercredi", "Jeudi", "Vendredi", "Samedi"]

  io = StringIO.new
  io << new_row

  d=0
    
  # Each day,date,game is a complete row
  (0..5).each do |n|

    # Resting is a special case
    if @item[:games][n][2] == "rest" then
      io << "<div class='col-md col-xs planning-day dayrest'>"
      io << days[n] << '<br>'
      io << "&nbsp;<br><strong>REPOS</strong>"
      io << "</div>"
    else
      io << "<div class='col-md col-xs planning-day day#{d}'>"
      io << days[n] << '<br>'
      io << @item[:games][n][0]  << '<br>'
      io << "<strong>" << check_service(@item[:games][n]) << "</strong><br>"
      io << game_slug(@item[:games][n][1])
      io << "</div>"
    end
    
    if d == 0
      d = 1
    else
      d = 0
    end
      
  end
  
  io << "</div>"

  ret = io.string
#  puts ret.encoding # returns UTF-8 on linux and CP850
   return ret.encode("UTF-8")
end
