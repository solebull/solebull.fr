# coding: utf-8
require "test/unit"

# require "yaml" # Needed to call game_list outside nanoc environment

require_relative "../lib/planning.rb"

# Test the game_list file's functions
class TestPlanning < Test::Unit::TestCase
  # Check the 'new_row' function
  def test_new_row
    assert_match(/.*div.*/, new_row())
    assert_match(/.*row.*/, new_row())
  end

  # Check the 'new_col' function
  def test_new_col
    ret = new_col("text", "style")
    assert_match(/.*col-xs.*/, ret)
    assert_match(/.*text.*/,   ret)
    assert_match(/.*style.*/,  ret)
  end

  # Check that the service_link() function returns some mandatory
  # service-related strings
  def test_service_link
    ret = service_link("serviceUrl", "serviceName")
    assert_match(/.*href.*/, ret)
    assert_match(/.*serviceUrl.*/, ret)
    assert_match(/.*serviceName.*/, ret)
  end

  # Check if the check_service(à function raise an error in case of
  # unknown service (/yt|twitch/)
  def test_check_service
    assert_match(/.*youtube.*/, check_service(['date', 'game', 'yt']))
    assert_match(/.*twitch.*/,  check_service(['date', 'game', 'twitch']))
    assert_raise(RuntimeError){ check_service(['date', 'game', 'unknown']) }
  end

end
