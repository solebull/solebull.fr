require "test/unit"

require_relative "../lib/menu.rb"

# Test case for the menu file
class TestMenu < Test::Unit::TestCase
  # Test that menu_item doesn't return a nil string
  def test_menu_item_not_null
    @item = Hash.new
    @item[:zer] = "aze"
    assert_not_nil(menu_item('aze', 'zer'))
  end

  # Check that the @item menu is not set to "active"
  def test_menu_item_not_active
    @item = Hash.new
    @item[:menu] = "azening"
    lab = menu_item("Planning", "ppl/")
    assert_not_match /active/, lab
  end

  # Check that the @item menu is set to "active"
  def test_menu_item_active
    @item = Hash.new
    @item[:menu] = "planning"
    lab = menu_item("Planning", "planning/")
    assert_match /active/, lab
  end

  # Check that the menu_item function set both 'nav-item' and 'nav-link'
  def test_menu_item_nav_item
    @item = Hash.new
    lab = menu_item("Planning", "planning/")
    assert_match /nav-item/, lab
    assert_match /nav-link/, lab
  end

  # Check we have li and /li markup
  def test_menu_item_li
    @item = Hash.new
    lab = menu_item("Planning", "planning/")
    assert_match /li/, lab
    assert_match /<\/li>/, lab
  end

  # Chack the menu() returned content
  def test_menu_content
    @item = Hash.new
    @config = Hash.new
    m = menu()
    assert_match /Accueil/, m
    assert_match /games/, m
  end

  # Check that planning link is shown according to config hash
  def test_menu_planning
    @item = Hash.new
    @config = Hash.new
    @config[:planning_show] = true
    m = menu()
    assert_match /planning/, m

    @config[:planning_show] = false
    m2 = menu()
    assert_not_match /planning/, m2
  end

  # Check that planning link is shown according to config hash
  def test_menu_minetest
    @item = Hash.new
    @config = Hash.new
    @config[:minetest_show] = true
    m = menu()
    assert_match /minetest/, m

    @config[:minetest_show] = false
    m2 = menu()
    assert_not_match /minetest/, m2
  end
end
